package usydtelcom.electric;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;


public class GreetingFragment extends Fragment {
    private Calendar calendar;
    private Globals g;
    private TextView confirm;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        g = (Globals)getActivity().getApplication();


    }

    public class getMac extends Thread {
        public void run(){


            while (g.getConnectionEstablished() == false){

                if (g.getMac() != null){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (g.getMac() == "Invalid"){

                                confirm.setText("Connection Failed Try Again");
                                g.setMac(null);
                            }else if(g.getMac() == "Connect" ) {
                                confirm.setText("Attempting to Connect");
                                g.setMac(null);
                            } else if(g.getMac()!=null){
                                confirm.setText("Connected to device: \n" + g.getMac());
                                g.setConnectionEstablished(true);
                            }
                        }
                    });
                }
            }
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        g.setConnectionEstablished(false);
        View view = inflater.inflate(R.layout.fragment_greeting, container, false);
        calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        TextView text = (TextView)view.findViewById(R.id.textView3);
        if (hour > 5 && hour < 12){
            text.setText("Good Morning");
        } else if (hour > 11 && hour < 18){
            text.setText("Good Afternoon");
        } else if (hour > 17 && hour < 22){
            text.setText("Good Evening");
        } else {
            text.setText("Good Night");
        }
        confirm = (TextView)view.findViewById(R.id.textView7);
        getMac m = new getMac();
        m.start();

        return view;

    }

}
