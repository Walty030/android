package usydtelcom.electric;

import android.animation.ValueAnimator;
import android.os.Bundle;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class CalorieFragment extends Fragment {
    private Globals g;
    private int rSit;
    private int rStand;
    private int rTotal;
    private int time;
    private String rJog;
    private String dTotal;
    private String sSit;
    private String sStand;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        g = (Globals)getActivity().getApplication();
        Long lSit = g.getLSit();
        Long lStand = g.getLStand();
        Double minutesSit = new Double(lSit/1000/60);
        Double minutesStand = new Double(lStand/1000/60);
        int cSit = minutesSit.intValue();
        if(cSit>59){
            int min1 = cSit%60;
            int hou1 = cSit/60;
            sSit = hou1 + "h " + min1 + "m";
        } else {
            sSit = cSit + "m";
        }
        int cStand = minutesStand.intValue();
        if(cStand>59){
            int min1 = cStand%60;
            int hou1 = cStand/60;
            sStand = hou1 + "h " + min1 + "m";
        } else {
            sStand = cStand + "m";
        }
        int cTotal = cSit + cStand;
        if (cTotal > 59){
            int min1 = cTotal%60;
            int hou1 = cTotal/60;
            dTotal = hou1 + "h " + min1 + "m";
        } else {
            dTotal = cTotal + "m";
        }

        double BMR;
        //Harris Benedict method
        if (g.getGender().equals("Male")){
            BMR = (float)(66.5 + (13.75 * Integer.parseInt(g.getWeight())) + (5.003 * Integer.parseInt(g.getHei())) - (6.755 * Integer.parseInt(g.getAge())));
        } else {
            BMR = (float)(665.1 + (9.563 * Integer.parseInt(g.getWeight())) + (1.85 * Integer.parseInt(g.getHei())) - (4.676 * Integer.parseInt(g.getAge())));
        }

        Double tSit = (1.2 *BMR)/60/24 * cSit;
        Double tStand = (1.375 * BMR)/60/24 *cStand;
        rSit = tSit.intValue();
        rStand = tStand.intValue();


        rTotal = rSit+rStand;
        //rTotal = 36000;
        double jog = (rTotal)/10;
        time = (int) jog;
        if (time > 59){
            int minutes = time%60;
            int hours = time/60;
            rJog = hours + "h " + minutes + "m";
        } else {
            rJog = time + "m";
        }


        getActivity().setTitle("Summary");


    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_calorie, container, false);



        Handler handler = new Handler();

        final TextView labelOne = (TextView)view.findViewById(R.id.textView9);
        labelOne.setVisibility(View.GONE);
        final View lineOne = view.findViewById(R.id.view2);
        lineOne.setVisibility(View.GONE);
        final TextView textOne = (TextView)view.findViewById(R.id.textView13);
        textOne.setVisibility(View.GONE);
        final TextView sitD = (TextView)view.findViewById(R.id.textView25);
        sitD.setVisibility(View.GONE);
        final TextView textTwo = (TextView)view.findViewById(R.id.textView12);
        textTwo.setVisibility(View.GONE);
        final TextView sit = (TextView)view.findViewById(R.id.textView8);
        sit.setVisibility(View.GONE);

        final TextView labelTwo = (TextView)view.findViewById(R.id.textView11);
        labelTwo.setVisibility(View.GONE);
        final View lineTwo = view.findViewById(R.id.view);
        lineTwo.setVisibility(View.GONE);
        final TextView textFive = (TextView)view.findViewById(R.id.textView28);
        textFive.setVisibility(View.GONE);
        final TextView standD = (TextView)view.findViewById(R.id.textView29);
        standD.setVisibility(View.GONE);
        final TextView textSix = (TextView)view.findViewById(R.id.textView27);
        textSix.setVisibility(View.GONE);
        final TextView stand = (TextView)view.findViewById(R.id.textView10);
        stand.setVisibility(View.GONE);

        final TextView totalCal = (TextView)view.findViewById(R.id.textTCal);
        totalCal.setVisibility(View.GONE);
        final TextView totalDur = (TextView)view.findViewById(R.id.textTDur);
        totalDur.setVisibility(View.GONE);
        final TextView textThree = (TextView)view.findViewById(R.id.textLabetT);
        textThree.setVisibility(View.GONE);

        final TextView run = (TextView)view.findViewById(R.id.textView14);
        run.setVisibility(View.GONE);
        final TextView textFour = (TextView)view.findViewById(R.id.textView15);
        textFour.setVisibility(View.GONE);
        final ImageView jogger = (ImageView)view.findViewById(R.id.imageView3);
        jogger.setVisibility(View.GONE);

        totalCal.setText(rTotal + "Cal");
        totalCal.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
        totalCal.setVisibility(getView().VISIBLE);
        totalDur.setText(dTotal);
        totalDur.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
        totalDur.setVisibility(getView().VISIBLE);

        textThree.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
        textThree.setVisibility(getView().VISIBLE);

        handler.postDelayed(new Runnable(){
            public void run(){
                labelOne.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                labelOne.setVisibility(getView().VISIBLE);
                lineOne.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                lineOne.setVisibility(getView().VISIBLE);
                textOne.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                textOne.setVisibility(getView().VISIBLE);
                sitD.setText(sSit);
                sitD.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                sitD.setVisibility(getView().VISIBLE);
                textTwo.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                textTwo.setVisibility(getView().VISIBLE);
                sit.setText(rSit + "Cal");
                sit.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                sit.setVisibility(getView().VISIBLE);

            }
        },500);

        handler.postDelayed(new Runnable(){
            public void run(){
                labelTwo.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                labelTwo.setVisibility(getView().VISIBLE);
                lineTwo.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                lineTwo.setVisibility(getView().VISIBLE);
                textFive.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                textFive.setVisibility(getView().VISIBLE);
                standD.setText(sStand);
                standD.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                standD.setVisibility(getView().VISIBLE);
                textSix.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                textSix.setVisibility(getView().VISIBLE);
                stand.setText(rStand + "Cal");
                stand.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                stand.setVisibility(getView().VISIBLE);
            }
        },1000);

        handler.postDelayed(new Runnable(){
            public void run(){
                animateTextView(0, time, run);
                //run.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                run.setVisibility(getView().VISIBLE);
                textFour.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                textFour.setVisibility(getView().VISIBLE);
                jogger.startAnimation(AnimationUtils.loadAnimation(getContext(),android.R.anim.slide_in_left));
                jogger.setVisibility(getView().VISIBLE);
            }
        },1500);





        // Inflate the layout for this fragment
        return view;
    }

    public void animateTextView(int initialValue, int finalValue, final TextView  textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1500);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = Integer.parseInt(valueAnimator.getAnimatedValue().toString());
                if(val > 59){
                    int min = val%60;
                    int hou = val/60;
                    textview.setText(hou + "h " + min + "m");
                } else {
                    textview.setText(val + "m");
                }

            }
        });
        valueAnimator.start();

    }
}
