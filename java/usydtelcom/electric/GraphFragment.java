package usydtelcom.electric;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PieChart;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;



public class GraphFragment extends Fragment {
    private final static UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    //private boolean stopGraph;
    private OutputStream outStream;
    private InputStream inStream;
    private TextView tSit;
    private TextView tStand;
    private String cSSit;
    private String cSStand;
    private Handler threadHandle;
    private String string;
    private Globals g;
    private int cSit;
    private int cStand;
    private FrameLayout layout;
    private View chartView;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //stopGraph = false;

        g = (Globals)getActivity().getApplication();
        //g.setStopGraph(stopGraph);
        outStream = g.getOutStream();
        Long lSit = g.getLSit();
        Long lStand = g.getLStand();
        Double minutesSit = new Double(lSit/1000/60);
        Double minutesStand = new Double(lStand/1000/60);
        cSit = minutesSit.intValue();
        cStand = minutesStand.intValue();
        getActivity().setTitle("Daily");

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_graph, container, false);

        layout = (FrameLayout)view.findViewById(R.id.chart);

        tSit = (TextView)view.findViewById(R.id.textViewSit);
        tSit.setText("Sit time: " + cSit);
        tStand = (TextView)view.findViewById(R.id.textViewStand);
        tStand.setText("Stand time: " + cStand);

        openChart();

//
//        Button btnChart = (Button) view.findViewById(R.id.btn_chart);
//        btnChart.setOnTouchListener(new View.OnTouchListener() {
//            //String s;
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//
//                        return true;
//
//
//                }
//                return false;
//
//            }
//        });



        // Inflate the layout for this fragment
        return view;
    }
    private void openChart(){
        if(chartView != null){
            layout.removeView(chartView);
        }


        //Pie Chart Section Names
        String[] code = new String[] {
                "Sitting", "Standing"
        };

        // Pie Chart Section Value
        int[] distribution = { cSit, cStand } ;

        // Color of each Pie Chart Sections
        int[] colors = { Color.YELLOW, Color.GREEN };

        if(cSit == 0 && cStand == 0){
            Toast.makeText(getContext(), "No Data for PIE Chart",
                    Toast.LENGTH_SHORT).show();
        } else if (cSit == 0){
            // Instantiating CategorySeries to plot Pie Chart
            CategorySeries distributionSeries = new CategorySeries("2016");

            // Adding a slice with its values and name to the Pie Chart
            distributionSeries.add(code[1], distribution[1]);


            // Instantiating a renderer for the Pie Chart
            DefaultRenderer defaultRenderer  = new DefaultRenderer();
            SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
            seriesRenderer.setColor(colors[1]);

            // Adding a renderer for a slice
            defaultRenderer.addSeriesRenderer(seriesRenderer);
            defaultRenderer.setLabelsTextSize(75);
            defaultRenderer.setLabelsColor(Color.BLACK);
            defaultRenderer.setChartTitle("Daily Activity (minutes)");
            defaultRenderer.setChartTitleTextSize(50);
            defaultRenderer.setPanEnabled(false);
            defaultRenderer.setZoomEnabled(false);
            defaultRenderer.setShowLegend(false);
            defaultRenderer.setDisplayValues(true);
            defaultRenderer.setStartAngle(90f);


            // Creating an intent to plot bar chart using dataset and multipleRenderer
            chartView = ChartFactory.getPieChartView(getContext(), distributionSeries, defaultRenderer);

            layout.addView(chartView);
        } else if (cStand == 0) {
            // Instantiating CategorySeries to plot Pie Chart
            CategorySeries distributionSeries = new CategorySeries("2016");

            // Adding a slice with its values and name to the Pie Chart
            distributionSeries.add(code[0], distribution[0]);

            // Instantiating a renderer for the Pie Chart
            DefaultRenderer defaultRenderer  = new DefaultRenderer();
            SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
            seriesRenderer.setColor(colors[0]);

            // Adding a renderer for a slice
            defaultRenderer.addSeriesRenderer(seriesRenderer);
            defaultRenderer.setInScroll(true);
            defaultRenderer.setLabelsTextSize(75);
            defaultRenderer.setLabelsColor(Color.BLACK);
            defaultRenderer.setChartTitle("Daily Activity (minutes)");
            defaultRenderer.setChartTitleTextSize(50);
            defaultRenderer.setPanEnabled(false);
            defaultRenderer.setZoomEnabled(false);
            defaultRenderer.setShowLegend(false);
            defaultRenderer.setDisplayValues(true);
            defaultRenderer.setStartAngle(90f);

            // Creating an intent to plot bar chart using dataset and multipleRenderer
            chartView = ChartFactory.getPieChartView(getContext(), distributionSeries, defaultRenderer);

            layout.addView(chartView);
        } else {
            // Instantiating CategorySeries to plot Pie Chart
            CategorySeries distributionSeries = new CategorySeries("2016");
            for(int i=0 ;i < distribution.length;i++){

                // Adding a slice with its values and name to the Pie Chart
                distributionSeries.add(code[i], distribution[i]);
            }
//
            // Instantiating a renderer for the Pie Chart
            DefaultRenderer defaultRenderer  = new DefaultRenderer();
            for(int i = 0 ;i<distribution.length;i++){
                SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
                seriesRenderer.setColor(colors[i]);

                // Adding a renderer for a slice
                defaultRenderer.addSeriesRenderer(seriesRenderer);
            }
            defaultRenderer.setLabelsTextSize(75);
            defaultRenderer.setInScroll(true);
            defaultRenderer.setLabelsColor(Color.BLACK);
            defaultRenderer.setChartTitle("Daily Activity (hours)");
            defaultRenderer.setChartTitleTextSize(50);
            defaultRenderer.setPanEnabled(false);
            defaultRenderer.setZoomEnabled(false);
            defaultRenderer.setShowLegend(false);
            defaultRenderer.setDisplayValues(true);

            // Creating an intent to plot bar chart using dataset and multipleRenderer
            chartView = ChartFactory.getPieChartView(getContext(), distributionSeries, defaultRenderer);

            layout.addView(chartView);
        }
    }
}
