package usydtelcom.electric;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.UUID;



public class USettingFragment extends Fragment {
    private final static UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private OutputStream outStream;
    private InputStream inStream;
    private TextView t;
    //private Handler threadHandle;
    private String string;
    Globals g;
    private Boolean isMale;

    private changeFragmentListener gB;
    public interface changeFragmentListener {
        public void goBack();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        g = (Globals)getActivity().getApplication();
        getActivity().setTitle("User Settings");

    }



    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_usetting, container, false);
        final NumberPicker np3 = (NumberPicker)view.findViewById(R.id.numberPicker3);
        final NumberPicker np4 = (NumberPicker)view.findViewById(R.id.numberPicker4);
        final NumberPicker np5 = (NumberPicker)view.findViewById(R.id.numberPicker5);
        ImageButton uBack = (ImageButton)view.findViewById(R.id.imageUBack);
        ImageButton uApply = (ImageButton)view.findViewById(R.id.imageUApply);

        final ImageButton fem = (ImageButton)view.findViewById(R.id.female);
        final ImageButton mal = (ImageButton)view.findViewById(R.id.male);

        np3.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        np4.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        np5.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        //AGE
        np3.setMinValue(1);
        np3.setMaxValue(100);

        //Height
        np4.setMinValue(1);
        np4.setMaxValue(220);

        //Weight
        np5.setMinValue(1);
        np5.setMaxValue(180);

        np3.setValue(Integer.parseInt(g.getAge()));
        np4.setValue(Integer.parseInt(g.getHei()));
        np5.setValue(Integer.parseInt(g.getWeight()));

        if (g.getGender().equals("Male")){
            mal.setBackgroundResource(R.drawable.male);
            isMale = true;
        } else {
            fem.setBackgroundResource(R.drawable.female);
            isMale = false;
        }

        mal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               if (isMale != true ){
                   mal.setBackgroundResource(R.drawable.male);
                   fem.setBackgroundResource(R.drawable.femalens);
                   isMale = true;
               }
            }
        });

        fem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (isMale == true ){
                    mal.setBackgroundResource(R.drawable.malens);
                    fem.setBackgroundResource(R.drawable.female);
                    isMale = false;
                }
            }
        });

        uBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                gB = (changeFragmentListener)getActivity();
                gB.goBack();
            }
        });

        uApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                g.setAge(Integer.toString(np3.getValue()));
                g.setHei(Integer.toString(np4.getValue()));
                g.setWeight(Integer.toString(np5.getValue()));
                if (isMale == true) {
                    g.setGender("Male");
                } else {
                    g.setGender("Female");
                }

                Toast.makeText(getContext(), "Saved Settings",
                        Toast.LENGTH_SHORT).show();

                gB = (changeFragmentListener)getActivity();
                gB.goBack();
            }
        });



//
//
//
//        Button save = (Button)view.findViewById(R.id.save);
//        save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                g.setSittingHeight(sit.getText().toString());
////                g.setStandingHeight(stand.getText().toString());
////
////                String s;
////                //Harris Benedict
////                if (m.isChecked()) {
////
////                    s = sit.getText().toString() + "\n" + stand.getText().toString() + "\n" +
////                            age.getText().toString() + "\n" + height.getText().toString() + "\n" +
////                            weight.getText().toString() + "\n" + "Male";
////                } else {
////
////                    s = sit.getText().toString() + "\n" + stand.getText().toString() + "\n" +
////                            age.getText().toString() + "\n" + height.getText().toString() + "\n" +
////                            weight.getText().toString() + "\n" + "Female";
////                }
//
//                g.setAge(age.getText().toString());
//                g.setHei(height.getText().toString());
//                g.setWeight(weight.getText().toString());
//
//                if(m.isChecked()){
//                    g.setGender("Male");
//                } else {
//                    g.setGender("Female");
//                }
//
//
////                writeToFile(s);
////
////                Toast.makeText(getContext(),  readFromFile(),
////                        Toast.LENGTH_SHORT).show();
//            }
//        });
//        // Inflate the layout for this fragment
        return view;
    }

//    private void writeToFile(String data) {
//        try {
//            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getContext().openFileOutput("config.txt", Context.MODE_PRIVATE));
//            outputStreamWriter.write(data + "\n");
//            outputStreamWriter.close();
//        }
//        catch (IOException e) {
//            Log.e("Exception", "File write failed: " + e.toString());
//        }
//    }
//
//    private String readFromFile() {
//
//        String ret = "";
//
//        try {
//            InputStream inputStream = getContext().openFileInput("config.txt");
//
//            if ( inputStream != null ) {
//                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                String receiveString = "";
//                StringBuilder stringBuilder = new StringBuilder();
//
//                while ( (receiveString = bufferedReader.readLine()) != null ) {
//                    stringBuilder.append(receiveString + "\n");
//                }
//
//                inputStream.close();
//                ret = stringBuilder.toString();
//            }
//        }
//        catch (FileNotFoundException e) {
//            Log.e("login activity", "File not found: " + e.toString());
//        } catch (IOException e) {
//            Log.e("login activity", "Can not read file: " + e.toString());
//        }
//
//        return ret;
//    }
//
//    /* Checks if external storage is available for read and write */
//    public boolean isExternalStorageWritable() {
//        String state = Environment.getExternalStorageState();
//        if (Environment.MEDIA_MOUNTED.equals(state)) {
//            return true;
//        }
//        return false;
//    }
//
//    /* Checks if external storage is available to at least read */
//    public boolean isExternalStorageReadable() {
//        String state = Environment.getExternalStorageState();
//        if (Environment.MEDIA_MOUNTED.equals(state) ||
//                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
//            return true;
//        }
//        return false;
//    }

}

