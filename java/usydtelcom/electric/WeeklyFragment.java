package usydtelcom.electric;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;
import java.util.List;


public class WeeklyFragment extends Fragment {
    private Globals g;
    private FrameLayout layout;
    private View chartView;
    private String[] day = new String[]{
      "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        g = (Globals)getActivity().getApplication();
        getActivity().setTitle("Weekly");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weekly, container, false);
        layout = (FrameLayout)view.findViewById(R.id.barchart);
        openChart();

        // Inflate the layout for this fragment
        return view;
    }

    private void openChart() {
        if (chartView != null) {
            layout.removeView(chartView);
        }


        //Pie Chart Section Names
        String[] titles = new String[]{
                "Sitting", "Standing"
        };


        int[] x = { 1,2,3,4,5,6,7 };
        int[] stand = {200,250,270,300,280,350,370};
        int[] sit = {220,270,290,280,260,300,330};

        // Creating an  XYSeries for Income
        XYSeries mStand = new XYSeries("Stand");
        // Creating an  XYSeries for Expense
        XYSeries mSit = new XYSeries("Sit");
        // Adding data to Income and Expense Series
        for(int i=0;i<x.length;i++){
            mStand.add(x[i], stand[i]);
            mSit.add(x[i],sit[i]);
        }

        // Creating a dataset to hold each series
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
        // Adding Income Series to the dataset
        dataset.addSeries(mStand);
        // Adding Expense Series to dataset
        dataset.addSeries(mSit);

        // Creating XYSeriesRenderer to customize incomeSeries
        XYSeriesRenderer standRenderer = new XYSeriesRenderer();
        standRenderer.setColor(Color.RED);
        standRenderer.setDisplayChartValues(true);
        standRenderer.setChartValuesTextSize(20);
        // Creating XYSeriesRenderer to customize expenseSeries
        XYSeriesRenderer sitRenderer = new XYSeriesRenderer();
        sitRenderer.setColor(Color.BLUE);
        sitRenderer.setDisplayChartValues(true);
        sitRenderer.setChartValuesTextSize(20);

        XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
        multiRenderer.setChartTitle("Weekly Activity");
        multiRenderer.setYTitle("Minutes", 0);
        multiRenderer.setAxisTitleTextSize(50);

        multiRenderer.setXLabelsColor(Color.BLACK);
        multiRenderer.setYLabelsColor(0, Color.BLACK);
        multiRenderer.setYAxisColor(Color.BLACK);
        multiRenderer.setXAxisColor(Color.BLACK);
        multiRenderer.setXLabels(0);
        multiRenderer.setYLabelsAlign(Paint.Align.RIGHT);

        //multiRenderer.setZoomButtonsVisible(true);
        for(int i=0;i<x.length;i++){
            multiRenderer.addXTextLabel(i+1, day[i]);
        }

        multiRenderer.addSeriesRenderer(standRenderer);
        multiRenderer.addSeriesRenderer(sitRenderer);
        multiRenderer.setZoomEnabled(false, false);
        multiRenderer.setPanEnabled(false);
        multiRenderer.setLegendTextSize(40);
        multiRenderer.setLabelsTextSize(20);
        multiRenderer.setLabelsColor(Color.BLACK);
        multiRenderer.setXAxisMin(0);
        multiRenderer.setXAxisMax(8);
        multiRenderer.setLegendHeight(100);
        multiRenderer.setChartTitleTextSize(50);
        multiRenderer.setMarginsColor(Color.WHITE);
        multiRenderer.setMargins(new int[] {100, 100, 200,0});
        multiRenderer.setBackgroundColor(Color.WHITE);



        chartView = ChartFactory.getBarChartView(getContext(), dataset, multiRenderer,
                BarChart.Type.DEFAULT);

        layout.addView(chartView);
    }



}
