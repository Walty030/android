package usydtelcom.electric;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ParseException;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Meeting extends Activity {

    public static ArrayList<String> nameOfEvent = new ArrayList<String>();
    public static ArrayList<String> startDates = new ArrayList<String>();
    public static ArrayList<String> endDates = new ArrayList<String>();
    public static ArrayList<String> descriptions = new ArrayList<String>();
    public static ArrayList<String> eventLocation = new ArrayList<String>();
    public static ArrayList<String> finalMeetingData = new ArrayList<String>();
    private Boolean flag;

    public ArrayList<String> readCalendarEvent(Context context) {

        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        //calendar.set(calendar.YEAR, calendar.MONTH, calendar.DAY_OF_MONTH, 0, 0, 0);

        long startDay = calendar.getTimeInMillis();

        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
//        Toast.makeText(this,  calendar.getTime().toString(),
//                Toast.LENGTH_SHORT).show();

        long endDay = calendar.getTimeInMillis();
//        Toast.makeText(this,  Long.toString(endDay),
//                Toast.LENGTH_SHORT).show();



        //String[] projection = new String[] { BaseColumns._ID, CalendarContract.Events.TITLE, CalendarContract.Events.DTSTART };





        //edit new String[] { "calendar_id", "title", "description",
        //"dtstart", "dtend", "eventLocation"

        String selection = CalendarContract.Events.DTSTART + " >= ? AND " + CalendarContract.Events.DTSTART + "<= ?";
        String[] selectionArgs = new String[] { Long.toString(startDay), Long.toString(endDay) };

        Cursor cursor = context.getContentResolver()
                .query(
                        Uri.parse("content://com.android.calendar/events"),
                        new String[] { "calendar_id", "title", "description",
                                "dtstart", "dtend", "eventLocation"}, selection,
                        selectionArgs, null);

        cursor.moveToFirst();
        // fetching calendars name
        String CNames[] = new String[cursor.getCount()];

        // fetching calendars id
        nameOfEvent.clear();
        startDates.clear();
        endDates.clear();
        descriptions.clear();
        eventLocation.clear();
        finalMeetingData.clear();
        for (int i = 0; i < CNames.length; i++) {

            nameOfEvent.add(cursor.getString(1));
            startDates.add(getDate(Long.parseLong(cursor.getString(3))));
            endDates.add(getDate(Long.parseLong(cursor.getString(4))));
            descriptions.add(cursor.getString(2));
            eventLocation.add(cursor.getString(5));
            finalMeetingData.add(cursor.getString(1)+"\n"+getDate(Long.parseLong(cursor.getString(3)))+" to\n"+getDate(Long.parseLong(cursor.getString(4)))+"\nInformation: "+cursor.getString(2)+"\nLocation: "+cursor.getString(5));
            CNames[i] = cursor.getString(1);
            cursor.moveToNext();

        }

        if (nameOfEvent.isEmpty()){
            finalMeetingData.add("No Meetings Today");
            flag = true;
        }

        return finalMeetingData;
    }

    public static String getDate(long milliSeconds) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd/MM/yyyy hh:mm:ss a");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting);

        flag = false;



        ArrayList<String> s = readCalendarEvent(this);

        ListView lv = (ListView)findViewById(R.id.meetings);
        List<String> your_array_list = new ArrayList<String>();
        your_array_list = s;

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                R.layout.mylist,
                your_array_list );

        lv.setAdapter(arrayAdapter);

        TextView t = (TextView)findViewById(R.id.advices);
        if (flag == true){
            t.setText("A regular day.");
        } else {
            t.setText("You will be having meetings today which could have long durations of sitting.\n\nTry to encourage yourself to stand more often for a healthy lifestyle.");


        }



//        ListView advice = (ListView)findViewById(R.id.advices);
//        advice.add
    }

}