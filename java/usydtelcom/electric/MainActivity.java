package usydtelcom.electric;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        ProfileFragment.changeFragmentListener, DSettingFragment.changeFragmentListener, USettingFragment.changeFragmentListener, HistoryFragment.changeFragmentListener  {
    private Globals g;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private Calendar cal;
    Fragment fragment, fragment2;
    //private String today;
    private final static UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private LinearLayout nmask;
    private Menu menu;
    private BluetoothAdapter bluetoothAdapter;
    private TextView adjust, profile, sport;
    private RelativeLayout fl, fl2;
    private String currentFragment;
    private static final int MY_PERMISSION_REQUEST_READ_CALENDAR = 123;
    private Boolean isFabOpen = false;
    private Boolean hasItConnected;
    private FloatingActionButton fab,fab1,fab2,fab3;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    private NavigationView navigationView;
    private Class fragmentClass;
    private String currentMac;
    private String checkMac;
    private String[] day = new String[]{
            "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };

    public class MacListenerThread extends Thread{
        public void run(){
            while (hasItConnected == false){
                if(checkMac!=null){
                    if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()){
                        currentMac = checkMac;
                        g.setTMac(null);
                        BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(currentMac);
                        //pop value out
                        ConnectingThread t = new ConnectingThread(bluetoothDevice);
                        t.start();
                    }
                } else {
                    SystemClock.sleep(100);
                }
                checkMac = g.getTMac();
            }
        }
    }

    private class ConnectingThread extends Thread {
        private final BluetoothSocket bluetoothSocket;
        private final BluetoothDevice bluetoothDevice;

        public ConnectingThread(BluetoothDevice device) {

            BluetoothSocket temp = null;
            bluetoothDevice = device;

            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                temp = bluetoothDevice.createRfcommSocketToServiceRecord(uuid);
            } catch (IOException e) {
                e.printStackTrace();
            }
            bluetoothSocket = temp;
        }

        public void run() {
            // Cancel discovery as it will slow down the connection
            bluetoothAdapter.cancelDiscovery();

            try {
                // This will block until it succeeds in connecting to the device
                // through the bluetoothSocket or throws an exception
                bluetoothSocket.connect();

            } catch (IOException connectException) {
                connectException.printStackTrace();
                try {
                    bluetoothSocket.close();
                } catch (IOException closeException) {
                    Toast.makeText(getApplicationContext(), "Cannot Connect",
                            Toast.LENGTH_SHORT).show();
                    closeException.printStackTrace();
                }
                if (!bluetoothAdapter.isEnabled()){
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Please turn on your bluetooth with the toggle button",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
//                    runOnUiThread(new Runnable() {
//                        public void run() {
//                            Toast.makeText(getApplicationContext(), "WTF is going on!!!",
//                                    Toast.LENGTH_SHORT).show();
//                        }
//                    });

                    g.setMac("Invalid");
                }
                return;
            }


            hasItConnected = true;

            //TODO add the mac address here


            g.setMac(currentMac);

//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    Toast.makeText(getApplicationContext(),g.getMac(), Toast.LENGTH_SHORT).show();
//                }
//            });


            editor.putString("Mac Address",currentMac);
            editor.apply();
            ConnectedThread s = new ConnectedThread(bluetoothSocket);
            s.start();
        }


        // Cancel an open connection and terminate the thread
        public void cancel() {
            try {
                bluetoothSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private OutputStream mmOutStream;
    private InputStream mmInStream;
    private String inputString;
    private String today;
    private String time;
    private String time2;
    private String past;
    private String tPast;
    private String[] fMatrix;
    private String[] fMatrix2;

    public class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;


            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;

            // g.setInputStream(mmInStream);
            String s = "S#";
            try {
                tmpOut.write(s.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
            today = g.getToday();
            //if the file was already created we need to calculate the standing and sitting duration
            if (fileExists(getApplicationContext(), "daily.txt")){
                final String duration = readFromFile("daily.txt");
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), duration,
                                Toast.LENGTH_SHORT).show();
                    }
                });
                final String lines[] = duration.split("\n");
                String matrix0[] = lines[0].split("\\s");

                for (int i=2; i<lines.length; i++){
                    String matrix[] = lines[i-1].split("\\s");
                    String matrix2[] = lines[i].split("\\s");
                    SimpleDateFormat simpleDateFormat =
                            new SimpleDateFormat("HH:mm:ss");

                    try {
                        Date date1 = simpleDateFormat.parse(matrix[0]);
                        Date date2 = simpleDateFormat.parse(matrix2[0]);
                        long difference = date2.getTime() - date1.getTime();
                        if(matrix[1].equals("Stand")){
                            g.setLStand(difference + g.getLStand());
                        } else if (matrix[1].equals("Sit")){
                            g.setLSit(difference + g.getLSit());
                        } else if (matrix[1].equals("Idle")){
                            //TODO
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    past = matrix2[1];
                    tPast = matrix2[0];
                }

                if (today.equals(matrix0[1])){

                } else {
                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("weekly.txt", Context.MODE_PRIVATE));
                        outputStreamWriter.write(matrix0[1] + " " + g.getLStand() + " " + g.getLSit() + "\n");
                        outputStreamWriter.close();
                    } catch (IOException e) {
                        Log.e("Exception", "File write failed: " + e.toString());
                    }
                    g.setLStand(0);
                    g.setLSit(0);
                    File dir = getFilesDir();
                    File d = new File(dir, "daily.txt");
                    d.delete();

                    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        Date checker = date.parse(matrix0[1]);
                        if (!isDateInCurrentWeek(checker)){
                            File w = new File(dir, "daily.txt");
                            w.delete();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }

            }

            g.setOutputStream(mmOutStream);
        }

        public void run(){

            while (true){


                try {
                    byte[] buffer = new byte[128];
                    String readMessage;
                    String pres;
                    int bytes;
                    if (mmInStream.available()>1) {
                        try {
                            // Read from the InputStream
                            bytes = mmInStream.read(buffer);
                            readMessage = new String(buffer, 0, bytes);
                            inputString = readMessage.trim();
                            cal = Calendar.getInstance();
                            int hour = cal.get(Calendar.HOUR_OF_DAY);
                            int minute = cal.get(Calendar.MINUTE);
                            int second = cal.get(Calendar.SECOND);
                            time = Integer.toString(hour)+":"+Integer.toString(minute)+":"+Integer.toString(second);

                            if(inputString.equals("I")){
                                pres = "Idle";
                                time2 = time + " Idle";
                            } else if(Integer.parseInt(inputString)>79){
                                pres = "Stand";
                                time2 = time + " Stand";
                            } else {
                                pres = "Sit";
                                time2 = time + " Sit";
                            }
                            if (!fileExists(getApplicationContext(), "daily.txt")) {
                                try {
                                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("daily.txt", Context.MODE_PRIVATE));
                                    outputStreamWriter.write("Daily " + today + "\n");
                                    outputStreamWriter.write(time2 + "\n");
                                    outputStreamWriter.close();
                                } catch (IOException e) {
                                    Log.e("Exception", "File write failed: " + e.toString());
                                }
                                //pres = time;
                            } else {
                                try {
                                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("daily.txt", Context.MODE_APPEND));
                                    outputStreamWriter.write(time2+"\n");
                                    outputStreamWriter.close();
                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
                                    try {
                                        Date date1 = simpleDateFormat.parse(tPast);
                                        Date date2 = simpleDateFormat.parse(time);
                                        long difference = date2.getTime() - date1.getTime();
                                        if(past.equals("Stand")){
                                            g.setLStand(difference + g.getLStand());
                                        } else if (past.equals("Sit")){
                                            g.setLSit(difference + g.getLSit());
                                        } else if (past.equals("Idle")){
                                            //TODO
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                                catch (IOException e) {
                                    Log.e("Exception", "File write failed: " + e.toString());
                                }
                            }
                            past = pres;
                            tPast = time;
                            g.setInputString(inputString);

                        } catch (IOException e) {
                            //Log.e(TAG, "disconnected", e);
                            break;
                        }

                    }    else
                        SystemClock.sleep(100);

                } catch (IOException e) {

                    e.printStackTrace();
                }
            }
        }
    }






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);



        //Request permission solved - WALTER :)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CALENDAR)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALENDAR},
                        MY_PERMISSION_REQUEST_READ_CALENDAR);

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALENDAR},
                        MY_PERMISSION_REQUEST_READ_CALENDAR);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }

        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        adjust = (TextView)findViewById(R.id.textAdjust);
        profile = (TextView)findViewById(R.id.textProfile);
        sport = (TextView)findViewById(R.id.textSport);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        g = (Globals)getApplication();
        g.setGSit(600);
        g.setGStand(120);

        fragment = null;
        g.setLStand(0);
        g.setLSit(0);

        hasItConnected = false;
        //this is when the fragment has not been replaced yet :)
        g.setStopAutomatic(true);
        try {
            fragment = GreetingFragment.class.newInstance();
            currentFragment = "Greetings";
        } catch (Exception e) {
            e.printStackTrace();
        }
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        nmask = (LinearLayout)findViewById(R.id.ll_mask);
        fl = (RelativeLayout)findViewById(R.id.flContent);
        fl2 = (RelativeLayout)findViewById(R.id.flContent2);
//

        fl2.setVisibility(View.INVISIBLE);

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        MacListenerThread m = new MacListenerThread();
        m.start();

        //TODO find the mac address

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.contains("Mac Address")) {
            Toast.makeText(this,"Bluetooth Devices",Toast.LENGTH_SHORT).show();

            String macText = prefs.getString("Mac Address", null);
            g.setTMac(macText);

        } else {
            Toast.makeText(this,"No Previous Bluetooth Devices",Toast.LENGTH_SHORT).show();
        }

        editor = prefs.edit();




        fab = (FloatingActionButton)findViewById(R.id.fab);
        fab1 = (FloatingActionButton)findViewById(R.id.fab1);
        fab2 = (FloatingActionButton)findViewById(R.id.fab2);
        fab3 = (FloatingActionButton)findViewById(R.id.fab3);
        fab_open = AnimationUtils.loadAnimation(this, R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(this,R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(this,R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(this,R.anim.rotate_backwards);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                animateFAB();

            }
        });
        fab1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                changeFragments(R.id.nav_manual);
                animateFAB();
            }
        });

        fab2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                changeFragments(R.id.nav_profile);
                animateFAB();
            }
        });
        fab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(g.getOutStream() == null){
                    Toast.makeText(getBaseContext(),  "Functionality Locked Connect to Smart Desk", Toast.LENGTH_SHORT).show();
                } else if (g.getStopAutomatic() == false) {
                    //if it has already been opened
                    fl.setVisibility(View.INVISIBLE);
                    fl2.setVisibility(View.VISIBLE);
                } else {
                    //CREATE INITIALISATION

                    try {
                        fragment2 = AutomaticFragment.class.newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    android.support.v4.app.FragmentManager fragmentManager2 = getSupportFragmentManager();
                    fragmentManager2.beginTransaction().replace(R.id.flContent2, fragment2).commit();

                    fl.setVisibility(View.INVISIBLE);
                    fl2.setVisibility(View.VISIBLE);
                }

                //changeFragments(R.id.nav_auto);
                animateFAB();
            }
        });
        if (!fileExists(this, "config.txt")){
            writeDefault();
        }

        //File Saved Settings
        String file = readFromFile("config.txt");
        String[] strArray = file.split("\n");


        String sit = strArray[0];
        String stand = strArray[1];
        String age = strArray[2];
        String height = strArray[3];
        String weight = strArray[4];
        String gender = strArray[5];

        //set to globals
        g.setSittingHeight(sit);
        g.setStandingHeight(stand);
        g.setAge(age);
        g.setWeight(weight);
        g.setHei(height);
        g.setGender(gender);

        cal = Calendar.getInstance();

        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        today = date;
//        int year = cal.get(Calendar.YEAR);
//        int month = cal.get(Calendar.MONTH) + 1;
//        int day = cal.get(Calendar.DAY_OF_MONTH);
       // today = Integer.toString(year) + Integer.toString(month) + Integer.toString(day);
        Toast.makeText(getApplicationContext(), today,
                Toast.LENGTH_SHORT).show();
        g.setToday(today);

    }


    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_bluetooth) {
            BluetoothFind();
        } else if (id == R.id.action_meeting){
            MeetingFind();
        }
        return super.onOptionsItemSelected(item);
    }

    private String readFromFile(String name) {

        String ret = "";

        try {
            InputStream inputStream = openFileInput(name);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString + "\n");
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }


    public static boolean isDateInCurrentWeek(Date date) {
        Calendar currentCalendar = Calendar.getInstance();
        int week = currentCalendar.get(Calendar.WEEK_OF_YEAR);
        int year = currentCalendar.get(Calendar.YEAR);
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);
        int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR);
        int targetYear = targetCalendar.get(Calendar.YEAR);
        return week == targetWeek && year == targetYear;
    }



    private void writeDefault() {
        Toast.makeText(this,  "Creating Default settings",
                Toast.LENGTH_SHORT).show();
        String data = "60\n" + "100\n" + "21\n" + "181\n" + "80\n" + "Male";

        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("config.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data + "\n");
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public boolean fileExists(Context context, String filename) {
        File file = context.getFileStreamPath(filename);
        if(file == null || !file.exists()) {
            return false;
        }
        return true;
    }

    private void BluetoothFind() {
        Intent i = new Intent(MainActivity.this, BluetoothActivity.class);

        startActivity(i);
    }

    private void MeetingFind() {
        Intent i = new Intent(MainActivity.this, Meeting.class);

        startActivity(i);
    }

    public void changeFragments(int id){



        /*
        This needs fixing i will move auto to a different fram layout
         */
        if (id == R.id.nav_auto) {
            if(g.getOutStream() == null){
                Toast.makeText(this,  "Functionality Locked Connect to Smart Desk",
                    Toast.LENGTH_SHORT).show();
            } else {
                fragmentClass = AutomaticFragment.class;
                currentFragment = "Controller";
                try {
                    fragment = (Fragment) fragmentClass.newInstance();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (id==R.id.nav_profile){
            //g.setStopAutomatic(true);


            currentFragment = "Profile";
            fragmentClass = ProfileFragment.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }

//        } else if (id == R.id.nav_settings) {
//
//            g.setStopAutomatic(true);
//            currentFragment = "USetting";
//            fragmentClass = USettingFragment.class;
//            try {
//                fragment = (Fragment) fragmentClass.newInstance();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }   else if (id == R.id.nav_manual) {

            //navigationView.getMenu().getItem(2).setChecked(true);
            //g.setStopAutomatic(true);
            currentFragment = "Sport";
            fragmentClass = SportFragment.class;
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }




//        } else if (id == R.id.nav_calorie) {
//            //navigationView.getMenu().getItem(3).setChecked(true);
//            g.setStopAutomatic(true);
//            currentFragment = "Calories";
//            fragmentClass = CalorieFragment.class;
//            try {
//                fragment = (Fragment) fragmentClass.newInstance();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } else if (id == R.id.nav_daily) {
//            g.setStopAutomatic(true);
//            currentFragment = "Daily";
//            fragmentClass = GraphFragment.class;
//            try {
//                fragment = (Fragment) fragmentClass.newInstance();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }  else if (id == R.id.nav_weekly){
//            g.setStopAutomatic(true);
//            currentFragment = "Weekly";
//            fragmentClass = WeeklyFragment.class;
//            try {
//                fragment = (Fragment) fragmentClass.newInstance();
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().disallowAddToBackStack();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        if (fl.getVisibility() == View.INVISIBLE){
            fl.setVisibility(View.VISIBLE);
            fl2.setVisibility(View.INVISIBLE);
        }
    }


    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        changeFragments(id);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void animateFAB(){
        //RelativeLayout fl = (RelativeLayout)findViewById(R.id.flContent);
//        WindowManager window = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
//
//        WindowManager.LayoutParams windowManager = getWindow().getAttributes();
//        windowManager.dimAmount = 0.75f;
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//        window.v
        //WindowManager.LayoutParams p = fl.;
        //p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;

        if(isFabOpen){
            fl.setClickable(true);
            fl.setAlpha(1f);
            nmask.setVisibility(View.INVISIBLE);
            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab3.startAnimation(fab_close);
            adjust.setVisibility(View.INVISIBLE);
            profile.setVisibility(View.INVISIBLE);
            sport.setVisibility(View.INVISIBLE);

            fab1.setClickable(false);
            fab2.setClickable(false);
            fab3.setClickable(false);
            fab.setImageResource(R.drawable.plus);
            isFabOpen = false;
            //fl.getForeground().setAlpha(0);

            //p.dimAmount = 1f;
            //getWindow().setAttributes(p);

        } else {
            fl.setClickable(false);
            fl.setAlpha(0.3f);
            nmask.setVisibility(View.VISIBLE);
            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            fab3.startAnimation(fab_open);
            adjust.setVisibility(View.VISIBLE);
            profile.setVisibility(View.VISIBLE);
            sport.setVisibility(View.VISIBLE);

            fab1.setClickable(true);
            fab2.setClickable(true);
            fab3.setClickable(true);
            fab.setImageResource(R.drawable.minus);
            isFabOpen = true;


        }
    }

    @Override
    protected void onStop(){
        super.onStop();
        cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);
        String time = Integer.toString(hour)+":"+Integer.toString(minute)+":"+Integer.toString(second);
        String time2 = time + " Idle";
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("daily.txt", Context.MODE_APPEND));
            outputStreamWriter.write(time2+"\n");
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_READ_CALENDAR: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Toast.makeText(this, "This Application has been granted permission to access the calendar",Toast.LENGTH_SHORT ).show();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    finish();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void ReplaceFragment(Class fragmentChange, String string) {

        g.setStopAutomatic(true);
        currentFragment = string;
        fragmentClass = fragmentChange;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
    }

    @Override
    public void goBack() {
        g.setStopAutomatic(true);
        currentFragment = "Profile";
        fragmentClass = ProfileFragment.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
    }
}

