package usydtelcom.electric;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.UUID;

public class AutomaticFragment extends Fragment {
    private final static UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private boolean stopAutomatic;
    private OutputStream outStream;
    private TextView t1;
    private TextView t2;
    private Globals g;
    Calendar cal;
    private int num;
    private int cSit;
    private int cStand;
    private Boolean memorypressed;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stopAutomatic = false;
        g = (Globals)getActivity().getApplication();
        outStream = g.getOutStream();
        g.setStopAutomatic(stopAutomatic);
        getActivity().setTitle("Microcontroller");
        memorypressed = false;
        cSit = Integer.parseInt(g.getSittingHeight().trim());
        cStand = Integer.parseInt(g.getStandingHeight().trim());

    }

    public class getData extends Thread{
        public void run(){



            while (stopAutomatic == false){

                if (g.getInputString()!=null){
                    num = Integer.parseInt(g.getInputString());
                    operationComplete();
                    g.setInputString(null);
                } else {
                    SystemClock.sleep(100);
                }

                stopAutomatic = g.getStopAutomatic();

            }
        }
    }
    public void operationComplete()
    {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (num < 100){
                    t1.setText("0" + Integer.toString(num));
                } else {
                    t1.setText(Integer.toString(num));
                }
                if (num > 79){
                    t2.setText("Standing");
                } else {
                    t2.setText("Sitting");
                }
            }
        });
    }

    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_automatic, container, false);

        final TextView sitText = (TextView) view.findViewById(R.id.textSitting);
        final TextView standText = (TextView) view.findViewById(R.id.textStanding);
        final ImageButton sit = (ImageButton)view.findViewById(R.id.imageButton_right);
        final ImageButton stand = (ImageButton)view.findViewById(R.id.imageButton_left);
        final ImageView controller = (ImageView)view.findViewById(R.id.controller);
        final ImageButton memory = (ImageButton)view.findViewById(R.id.imageButton_centre);
        final ImageButton up = (ImageButton)view.findViewById(R.id.imageButton_up);
        final ImageButton down = (ImageButton)view.findViewById(R.id.imageButton_down);

        t1 = (TextView) view.findViewById(R.id.height);
        t2 = (TextView) view.findViewById(R.id.status);
        if (cSit < 100){
            sitText.setText("0"+Integer.toString(cSit));
        } else {
            sitText.setText(Integer.toString(cSit));
        }

        standText.setText(Integer.toString(cStand));


        standText.setVisibility(getView().INVISIBLE);
        sitText.setVisibility(getView().INVISIBLE);

        memory.setOnClickListener(new View.OnClickListener() {
            String s;
            @Override
            public void onClick(View v) {
                if(memorypressed==false){
                    controller.setBackgroundResource(R.drawable.memory_pressed);
                    standText.setVisibility(getView().VISIBLE);
                    sitText.setVisibility(getView().VISIBLE);
                    memorypressed = true;
                } else {
                    controller.setBackgroundResource(R.drawable.not_pressed);
                    standText.setVisibility(getView().INVISIBLE);
                    sitText.setVisibility(getView().INVISIBLE);
                    memorypressed = false;
                }
            }
        });

        sit.setOnClickListener(new View.OnClickListener() {
            String s;
            @Override
            public void onClick(View v) {
                if(memorypressed==true){
                    controller.setBackgroundResource(R.drawable.not_pressed);
                    cSit = num;
                    g.setSittingHeight(Integer.toString(num));
                    standText.setVisibility(getView().INVISIBLE);
                    sitText.setVisibility(getView().INVISIBLE);
                    memorypressed = false;
                    sitText.setText(t1.getText());
                    Toast.makeText(getContext(), "Saved Sitting Height",
                        Toast.LENGTH_SHORT).show();
                } else {
                    controller.setBackgroundResource(R.drawable.right_pressed);
                    t2.setText("Adjusting");
                    s = "C"+cSit+"#";
                    try {
                        outStream.write(s.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        stand.setOnClickListener(new View.OnClickListener() {
            String s;
            @Override
            public void onClick(View v) {
                if(memorypressed==true){
                    controller.setBackgroundResource(R.drawable.not_pressed);
                    cStand = num;
                    g.setStandingHeight(Integer.toString(num));
                    standText.setVisibility(getView().INVISIBLE);
                    sitText.setVisibility(getView().INVISIBLE);
                    memorypressed = false;
                    standText.setText(t1.getText());
                    Toast.makeText(getContext(), "Saved Standing Height",
                        Toast.LENGTH_SHORT).show();
                } else {
                    controller.setBackgroundResource(R.drawable.left_pressed);
                    t2.setText("Adjusting");
                    s = "C"+cStand+"#";
                    try {
                        outStream.write(s.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        up.setOnTouchListener(new View.OnTouchListener() {
            String s;
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if(memorypressed==true){
                            standText.setVisibility(getView().INVISIBLE);
                            sitText.setVisibility(getView().INVISIBLE);
                            memorypressed = false;
                        }
                        controller.setBackgroundResource(R.drawable.up_pressed);

                        s = "U#";
                        t2.setText("Adjusting");
                        try {
                            outStream.write(s.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        return true;

                    case MotionEvent.ACTION_UP:
                        controller.setBackgroundResource(R.drawable.not_pressed);
                        s = "S#";
                        try {
                            outStream.write(s.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                       return true;

                }
                return false;

            }
        });

        down.setOnTouchListener(new View.OnTouchListener() {
            String s;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if(memorypressed==true){
                            standText.setVisibility(getView().INVISIBLE);
                            sitText.setVisibility(getView().INVISIBLE);
                            memorypressed = false;
                        }

                        controller.setBackgroundResource(R.drawable.down_pressed);
                        s = "D#";
                        t2.setText("Adjusting");
                        try {
                            outStream.write(s.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        return true;

                    case MotionEvent.ACTION_UP:
                        controller.setBackgroundResource(R.drawable.not_pressed);
                        s = "S#";
                        try {
                            outStream.write(s.getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return true;

                }
                return false;

            }
        });




        getData s = new getData();
        s.start();

        return view;
    }


}

