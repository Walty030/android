package usydtelcom.electric;

import android.content.Context;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.IOException;


public class ProfileFragment extends Fragment {
    changeFragmentListener methodToChange;
    private Class fragmentClass;
    public interface changeFragmentListener {
        public void ReplaceFragment(Class fragmentChange, String string);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Profile");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ImageButton deskSetting = (ImageButton) view.findViewById(R.id.imagebutton_green);
        ImageButton userSetting = (ImageButton) view.findViewById(R.id.imagebutton_blue);
        ImageButton userManual = (ImageButton) view.findViewById(R.id.imagebutton_purple);
        ImageButton history = (ImageButton) view.findViewById(R.id.imagebutton_violet);
        ImageButton analysis = (ImageButton) view.findViewById(R.id.imagebutton_brown);
        deskSetting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                    methodToChange =(changeFragmentListener)getActivity();
                    fragmentClass = DSettingFragment.class;
                    methodToChange.ReplaceFragment(fragmentClass, "DSetting");
            }
        });
        userSetting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                methodToChange =(changeFragmentListener)getActivity();
                fragmentClass = USettingFragment.class;
                methodToChange.ReplaceFragment(fragmentClass, "USetting");
            }
        });
        userManual.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                methodToChange =(changeFragmentListener)getActivity();
                fragmentClass = ManualFragment.class;
                methodToChange.ReplaceFragment(fragmentClass, "Manual");
            }
        });
        history.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                methodToChange =(changeFragmentListener)getActivity();
                fragmentClass = HistoryFragment.class;
                methodToChange.ReplaceFragment(fragmentClass, "History");
            }
        });
        analysis.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                methodToChange =(changeFragmentListener)getActivity();
                fragmentClass = AnalysisFragment.class;
                methodToChange.ReplaceFragment(fragmentClass, "Analysis");
            }
        });

        return view;
    }


}
