package usydtelcom.electric;

import android.app.Application;
import android.bluetooth.BluetoothSocket;
import android.renderscript.ScriptGroup;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Walter on 18/04/2016.
 */
public class Globals extends Application {
    private String gInputString;
    private OutputStream globalOutStream;
    private String globalMac;
    private String globalTMac;
    private String globalSitHeight;
    private String globalStandHeight;
    private Boolean globalStopManual;
    private Boolean globalStopAutomatic;
    private Boolean globalStopGraph;
    private Boolean connectionEstablished;
    private int gsit;
    private int gstand;
    private long lsit;
    private long lstand;

    private double gBMR;

    private String gAge;
    private String gHeight;
    private String gWeight;
    private String gGender;
    private String gToday;
    private String gPast;
    private String gTPast;


    public void setPast(String past){
        gPast = past;
    }

    public String getPast(){
        return gPast;
    }

    public void setTPast(String tPast){
        gTPast = tPast;
    }

    public String getTPast(){
        return gTPast;
    }

    public void setToday(String today){
        gToday = today;
    }

    public String getToday(){
        return gToday;
    }


    public void setAge(String age) {
        gAge = age;
    }

    public String getAge(){
        return gAge;
    }

    public void setHei(String height) {
        gHeight = height;
    }

    public String getHei(){
        return gHeight;
    }

    public void setWeight(String weight) {
        gWeight = weight;
    }

    public String getWeight(){
        return gWeight;
    }

    public void setGender(String gender) {
        gGender = gender;
    }

    public String getGender(){
        return gGender;
    }


    public void setBMR(double BMR){
        gBMR = BMR;
    }

    public double getBMR(){
        return gBMR;
    }

    public void setConnectionEstablished(Boolean connection){
        connectionEstablished = connection;
    }

    public Boolean getConnectionEstablished(){
        return connectionEstablished;
    }

    //sitting duration
    public void setGSit(int cSit){
        gsit = cSit;
    }

    public int getGSit(){
        return gsit;
    }

    //standing duration
    public void setGStand(int cStand){
        gstand = cStand;
    }

    public int getGStand(){
        return gstand;
    }

    //sitting duration
    public void setLSit(long cSit){
        lsit = cSit;
    }

    public long getLSit(){
        return lsit;
    }

    //standing duration
    public void setLStand(long cStand){
        lstand = cStand;
    }

    public long getLStand(){
        return lstand;
    }

    public void setMac(String mac){
        this.globalMac = mac;
    }

    public String getMac(){
        return globalMac;
    }

    public void setTMac(String tMac){
        this.globalTMac = tMac;
    }

    public String getTMac(){
        return globalTMac;
    }

    public void setInputString(String inputString){
        gInputString = inputString;
    }

    public String getInputString(){
        return gInputString;
    }

    public void setOutputStream(OutputStream outStream){
        globalOutStream = outStream;
    }

    public OutputStream getOutStream(){
        return globalOutStream;
    }

    public void setSittingHeight(String sit){
        globalSitHeight = sit;
    }

    public String getSittingHeight() {
        return globalSitHeight;
    }

    public void setStandingHeight(String stand){
        globalStandHeight = stand;
    }

    public String getStandingHeight() {
        return globalStandHeight;
    }

    public void setStopManual(Boolean stopManual){
        globalStopManual = stopManual;
    }

    public Boolean getStopManual() {
        return globalStopManual;
    }

    public void setStopAutomatic(Boolean stopAutomatic){
        globalStopAutomatic = stopAutomatic;
    }

    public Boolean getStopAutomatic() {
        return globalStopAutomatic;
    }

    public void setStopGraph(Boolean stopGraph){
        globalStopGraph = stopGraph;
    }

    public Boolean getStopGraph() {
        return globalStopGraph;
    }

}
