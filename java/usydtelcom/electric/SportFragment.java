package usydtelcom.electric;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.OutputStream;
import java.util.Calendar;
import java.util.UUID;


public class SportFragment extends Fragment {
    //    BluetoothAdapter bluetoothAdapter;
    private final static UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private boolean stopManual;
    private OutputStream outStream;
    private TextView t;
    private Globals g;
    private Calendar calendar;
    private int num;
    private int hou;
    private int min;
    private int current;
    private Boolean startpressed;
    private int started;
    private long max;
    private FloatingActionButton fab,fab1,fab2;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //stopManual = false;
        getActivity().setTitle("Sport");
        g = (Globals)getActivity().getApplication();
        g.setStopManual(stopManual);
        outStream = g.getOutStream();
        startpressed  = false;

    }
//    public class getData extends Thread{
//        public void run(){
//
//            while (stopManual == false){
//
//                if (g.getInputString()!=null){
//                    num = Integer.parseInt(g.getInputString());
//                    operationComplete();
//                    g.setInputString(null);
//                } else {
//                    SystemClock.sleep(100);
//                }
//
//                stopManual = g.getStopManual();
//
//            }
//        }
//    }
//    public void operationComplete()
//    {
//        getActivity().runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if (num < 100){
//                    t.setText("0" + Integer.toString(num));
//                } else {
//                    t.setText(Integer.toString(num));
//                }
//            }
//        });
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_sport, container, false);

        final TextView textPer = (TextView)view.findViewById(R.id.textPer);
        final TextView textCal = (TextView)view.findViewById(R.id.textCal);
        final TextView textHour = (TextView)view.findViewById(R.id.textHour);
        final TextView textMin = (TextView)view.findViewById(R.id.textMinute);
        final TextView target = (TextView)view.findViewById(R.id.textView2);
        //final TextView setTimeLabel = (TextView)view.findViewById(R.id.textView16);
        final ImageButton upHour = (ImageButton)view.findViewById(R.id.uphour);
        final ImageButton downHour = (ImageButton)view.findViewById(R.id.downhour);
        final ImageButton upMinute = (ImageButton)view.findViewById(R.id.upminute);
        final ImageButton downMinute = (ImageButton)view.findViewById(R.id.downminute);
        final ImageView button = (ImageView)view.findViewById(R.id.imageTimerButtons);
        final ImageView start = (ImageView)view.findViewById(R.id.coloredButton);

        upHour.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        hou = hou + 1;
                        if (hou == 13){
                            hou = 0;
                        }
                        if ( hou < 10){
                            textHour.setText("0" + hou);
                        } else {
                            textHour.setText(hou + "");
                        }

                        button.setBackgroundResource(R.drawable.uphourbutton);
                        return true;

                    case MotionEvent.ACTION_UP:
                        button.setBackgroundResource(R.drawable.notbutton);
                        return true;

                }
                return false;
            }
        });

        downHour.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        hou = hou - 1;
                        if (hou == -1){
                            hou = 12;
                        }

                        if ( hou < 10){
                            textHour.setText("0" + hou);
                        } else {
                            textHour.setText(hou + "");
                        }

                        button.setBackgroundResource(R.drawable.downhourbutton);
                        return true;

                    case MotionEvent.ACTION_UP:
                        button.setBackgroundResource(R.drawable.notbutton);
                        return true;

                }
                return false;
            }
        });

        upMinute.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        min = min + 5;
                        if (min == 60) {
                            min = 0;
                        }

                        if ( min < 10){
                            textMin.setText("0" + min);
                        } else {
                            textMin.setText(min + "");
                        }

                        button.setBackgroundResource(R.drawable.upminutebutton);
                        return true;

                    case MotionEvent.ACTION_UP:
                        button.setBackgroundResource(R.drawable.notbutton);
                        return true;

                }
                return false;
            }
        });

        downMinute.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        min = min - 5;
                        if (min == -5){
                            min = 55;
                        }

                        if ( min < 10){
                            textMin.setText("0" + min);
                        } else {
                            textMin.setText(min + "");
                        }

                        button.setBackgroundResource(R.drawable.downminutebutton);
                        return true;

                    case MotionEvent.ACTION_UP:
                        button.setBackgroundResource(R.drawable.notbutton);
                        return true;

                }
                return false;
            }
        });



        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);



        start.setOnClickListener(new View.OnClickListener() {
            String s;
            @Override
            public void onClick(View v) {
                if (hou == 0 && min == 0){
                    Toast.makeText(getContext(), "Invalid Time",
                        Toast.LENGTH_SHORT).show();
                } else if(startpressed==true){
                    progressBar.setProgress(0);
                    startpressed = false;
                    textPer.setText("Ready!");
                    target.setVisibility(View.INVISIBLE);
                    button.setVisibility(View.VISIBLE);
                    min = 0;
                    hou = 0;
                    textHour.setText("00");
                    textMin.setText("00");
                    textCal.setText("0 Cal Expected");
                    start.setBackgroundResource(R.drawable.startbutton);
                    upHour.setVisibility(View.VISIBLE);
                    downHour.setVisibility(View.VISIBLE);
                    upMinute.setVisibility(View.VISIBLE);
                    downMinute.setVisibility(View.VISIBLE);
                } else {
                    upHour.setVisibility(View.INVISIBLE);
                    downHour.setVisibility(View.INVISIBLE);
                    upMinute.setVisibility(View.INVISIBLE);
                    downMinute.setVisibility(View.INVISIBLE);
                    startpressed = true;
                    if (hou == 0) {
                        started = min;
                    } else {
                        started = hou*60+min;
                    }
                    max = started*60000;
                    button.setVisibility(View.INVISIBLE);
                    textPer.setText("Finish 0%");
                    target.setVisibility(View.VISIBLE);
                    target.setText("Target: " + textHour.getText() + "h " + textMin.getText() + "m");
                    textHour.setText("00");
                    textMin.setText("00");
                    //for margin
                    textCal.setText("0 Cal");
                    start.setBackgroundResource(R.drawable.stopbutton);
                    min = 0;
                    hou = 0;
                    num = 0;

                    CountDownTimer countDownTimer = new CountDownTimer(max, 60000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            if(startpressed == false)
                            {
                                cancel();
                            } else {
                                min = min + 1;
                                if (min == 60){
                                    hou = hou + 1;
                                    min = 0;
                                    textMin.setText("0" + min);
                                    if ( hou < 10){
                                        textHour.setText("0" + hou);
                                    } else {
                                        textHour.setText(hou + "");
                                    }
                                } else if ( min < 10){
                                    textMin.setText("0" + min);
                                } else {
                                    textMin.setText(min + "");
                                }
                                num = num + 1;
//
//                                //percentile
                                double perc = (double)num/started * 100;
                                int rounds = (int) Math.round(perc);
                                textPer.setText("Finish " + String.format("%.1f", perc) + "%");


                                //textPer.setText("Finish " + num*10 + "%");
                                textCal.setText(num*20 + "Cal");

                                progressBar.setProgress(rounds);
                            }
                        }

                        @Override
                        public void onFinish() {
                            min = min + 1;
                            if (min == 60){
                                hou = hou + 1;
                                min = 0;
                                textMin.setText("0" + min);
                                if ( hou < 10){
                                    textHour.setText("0" + hou);
                                } else {
                                    textHour.setText(hou + "");
                                }
                            } else if ( min < 10){
                                textMin.setText("0" + min);
                            } else {
                                textMin.setText(min + "");
                            }
                            num = num + 1;
//
//                            //percentile
                            double perc = (double)num/started * 100;
                            int rounds = (int) Math.round(perc);
                            textPer.setText("Finish " + String.format("%.1f", perc) + "%");


                            //textPer.setText("Finish " + num*10 + "%");
                            textCal.setText(num*20 + "Cal");

                            progressBar.setProgress(rounds);
                            //textTime.setText(num*0.1 + "");
                            //t.setText("DONE!!");
                            playSound();
                        }
                    };

                    countDownTimer.start();
                    //ObjectAnimator animation = ObjectAnimator.ofInt (progressBar, "progress", 0, 500); // see this max value coming back here, we animale towards that value
//        animation.setDuration (5000); //in milliseconds
//        animation.setInterpolator (new DecelerateInterpolator());
//        animation.start ();


                }
            }
        });





       //progressBar.incrementProgressBy(1);
//

       //countDownTimer.start();

//        final ImageButton up = (ImageButton)view.findViewById(R.id.buttonup);
//
//        fab = (FloatingActionButton)view.findViewById(R.id.fab);
//        fab1 = (FloatingActionButton)view.findViewById(R.id.fab1);
//        fab2 = (FloatingActionButton)view.findViewById(R.id.fab2);
//        fab_open = AnimationUtils.loadAnimation(getContext(), R.anim.fab_open);
//        fab_close = AnimationUtils.loadAnimation(getContext(),R.anim.fab_close);
//        rotate_forward = AnimationUtils.loadAnimation(getContext(),R.anim.rotate_forward);
//        rotate_backward = AnimationUtils.loadAnimation(getContext(),R.anim.rotate_backwards);
//        fab.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v)
//            {
//                animateFAB();
//            }
//        });
//        fab1.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v)
//            {
//                if(num!=0){
//                    g.setStandingHeight(Integer.toString(num));
//                }
//                Toast.makeText(getContext(), "Saved standing height",
//                        Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        fab2.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v)
//            {
//                if(num!=0){
//                    g.setSittingHeight(Integer.toString(num));
//                }
//                Toast.makeText(getContext(), "Saved sitting height",
//                        Toast.LENGTH_SHORT).show();
//            }
//        });
//        up.setOnTouchListener(new View.OnTouchListener() {
//            String s;
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        up.setBackgroundResource(R.drawable.uppressed);
//                        calendar = Calendar.getInstance();
//                        s = "U#";
//                        t.setText("REQ");
//                        try {
//                            outStream.write(s.getBytes());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//
//                        return true;
//
//                    case MotionEvent.ACTION_UP:
//                        up.setBackgroundResource(R.drawable.up);
//                        s = "S#";
//                        calendar = Calendar.getInstance();
//                        try {
//                            outStream.write(s.getBytes());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        return true;
//
//                }
//                return false;
//
//            }
//        });
//        final ImageButton down = (ImageButton)view.findViewById(R.id.buttondown);
//        down.setOnTouchListener(new View.OnTouchListener() {
//            String s;
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        down.setBackgroundResource(R.drawable.downpressed);
//                        s = "D#";
//                        t.setText("REQ");
//                        try {
//                            outStream.write(s.getBytes());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//
//                        return true;
//
//                    case MotionEvent.ACTION_UP:
//                        down.setBackgroundResource(R.drawable.down);
//                        s = "S#";
//                        calendar = Calendar.getInstance();
//                        try {
//                            outStream.write(s.getBytes());
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                        return true;
//
//                }
//                return false;
//
//            }
//        });
//        getData s = new getData();
//        s.start();

        // Inflate the layout for this fragment
        return view;
    }

    public void playSound() {

        MediaPlayer mp = MediaPlayer.create(getContext(), R.raw.alarm); //replace 'sound' by your    music/sound
        mp.start();

    }

//    public void animateFAB(){
//
//        if(isFabOpen){
//
//            fab.startAnimation(rotate_backward);
//            fab1.startAnimation(fab_close);
//            fab2.startAnimation(fab_close);
//            fab1.setClickable(false);
//            fab2.setClickable(false);
//            fab.setImageResource(R.drawable.plus);
//            isFabOpen = false;
//
//        } else {
//
//            fab.startAnimation(rotate_forward);
//            fab1.startAnimation(fab_open);
//            fab2.startAnimation(fab_open);
//            fab1.setClickable(true);
//            fab2.setClickable(true);
//            fab.setImageResource(R.drawable.minus);
//            isFabOpen = true;
//        }
//    }
}
