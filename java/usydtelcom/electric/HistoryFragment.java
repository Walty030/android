package usydtelcom.electric;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.achartengine.ChartFactory;
import org.achartengine.chart.BarChart;
import org.achartengine.model.CategorySeries;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.math.BigDecimal;


public class HistoryFragment extends Fragment {
    private Globals g;
    private int rSit;
    private int rStand;
    private int rTotal;
    private int time;
    private TextView labelSi;
    private TextView labelSta;
    private TextView labelCa;
    private TextView labelJo;
    private FrameLayout layout;
    private FrameLayout layout2;
    private View chartView;
    private View chartView2;
    private String[] days = new String[]{
            "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
    };

    private String rJog;
    private String dTotal;
    private String sSit;
    private String sStand;
    private Boolean isDay;

    private changeFragmentListener gB;
    public interface changeFragmentListener {
        public void goBack();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        g = (Globals)getActivity().getApplication();

        getActivity().setTitle("History");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        layout = (FrameLayout)view.findViewById(R.id.chart2);
        layout2 = (FrameLayout)view.findViewById(R.id.chart3);
        labelSi = (TextView)view.findViewById(R.id.textViewSi);
        labelSta = (TextView)view.findViewById(R.id.textViewSta);
        labelCa = (TextView)view.findViewById(R.id.textViewCa);
        labelJo = (TextView)view.findViewById(R.id.textViewJo);
        final TextView labelDorW = (TextView)view.findViewById(R.id.dorw);
        final ViewGroup.LayoutParams params = layout.getLayoutParams();
        final ViewGroup.LayoutParams params2 = layout2.getLayoutParams();
        ImageButton back = (ImageButton)view.findViewById(R.id.imageButtonB);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gB = (changeFragmentListener)getActivity();
                gB.goBack();
            }
        });
//        Toast.makeText(getContext(), params.height + "",
//                Toast.LENGTH_SHORT).show();
        params.height=1;
        params2.height=1;
        layout.setLayoutParams(params);
        layout2.setLayoutParams(params2);

        final FloatingActionButton day = (FloatingActionButton)view.findViewById(R.id.fabDay);
        day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDay = true;
                computeValues(3600000, 28800000);
                labelDorW.setText("Daily");
                params.height=1440;
                params2.height=1;
                layout.setLayoutParams(params);
                layout2.setLayoutParams(params2);
            }
        });

        final FloatingActionButton week = (FloatingActionButton)view.findViewById(R.id.fabWeek);
        week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isDay = false;
                computeValues(36000000, 115200000);
                labelDorW.setText("Weekly");
                params.height=1440;
                params2.height=1440;
                layout.setLayoutParams(params);
                layout2.setLayoutParams(params2);
            }
        });

        return view;
    }

    public void computeValues(long stand, long sit){
        Long lSit = sit;
        Long lStand = stand;
        Double minutesSit = new Double(lSit/1000/60);
        Double minutesStand = new Double(lStand/1000/60);
        Double hourSit = new BigDecimal(minutesSit/60).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
        Double hourStand = new BigDecimal(minutesStand/60).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();

        int cSit = minutesSit.intValue();
        if(cSit>59){
            int min1 = cSit%60;
            int hou1 = cSit/60;
            sSit = hou1 + "h " + min1 + "m";
        } else {
            sSit = cSit + "m";
        }
        int cStand = minutesStand.intValue();
        if(cStand>59){
            int min1 = cStand%60;
            int hou1 = cStand/60;
            sStand = hou1 + "h " + min1 + "m";
        } else {
            sStand = cStand + "m";
        }
        int cTotal = cSit + cStand;
        if (cTotal > 59){
            int min1 = cTotal%60;
            int hou1 = cTotal/60;
            dTotal = hou1 + "h " + min1 + "m";
        } else {
            dTotal = cTotal + "m";
        }

        double BMR;
        //Harris Benedict method
        if (g.getGender().equals("Male")){
            BMR = (float)(66.5 + (13.75 * Integer.parseInt(g.getWeight())) + (5.003 * Integer.parseInt(g.getHei())) - (6.755 * Integer.parseInt(g.getAge())));
        } else {
            BMR = (float)(665.1 + (9.563 * Integer.parseInt(g.getWeight())) + (1.85 * Integer.parseInt(g.getHei())) - (4.676 * Integer.parseInt(g.getAge())));
        }

        Double tSit = (1.2 *BMR)/60/24 * cSit;
        Double tStand = (1.375 * BMR)/60/24 *cStand;
        rSit = tSit.intValue();
        rStand = tStand.intValue();


        rTotal = rSit+rStand;
        //rTotal = 36000;
        double jog = (rTotal)/10;
        time = (int) jog;
        if (time > 59){
            int minutes = time%60;
            int hours = time/60;
            rJog = hours + "h " + minutes + "m";
        } else {
            rJog = time + "m";
        }

        labelSi.setText(sSit);
        labelSta.setText(sStand);
        labelCa.setText(rTotal + "Cal");
        animateTextView(0, time, labelJo);

        openChart(hourSit, hourStand);
    }

    public void animateTextView(int initialValue, int finalValue, final TextView  textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(1500);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = Integer.parseInt(valueAnimator.getAnimatedValue().toString());
                if(val > 59){
                    int min = val%60;
                    int hou = val/60;
                    textview.setText(hou + "h " + min + "m");
                } else {
                    textview.setText(val + "m");
                }

            }
        });
        valueAnimator.start();

    }

    private void openChart(double cSit, double cStand){
        if(chartView != null){
            layout.removeView(chartView);
        }


        //Pie Chart Section Names
        String[] code = new String[] {
                "Sitting", "Standing"
        };

        // Pie Chart Section Value
        Double[] distribution = { cSit, cStand } ;

        // Color of each Pie Chart Sections
        int[] colors = { Color.YELLOW, Color.GREEN };

        if(cSit == 0 && cStand == 0){
            Toast.makeText(getContext(), "No Data for PIE Chart",
                    Toast.LENGTH_SHORT).show();
        } else if (cSit == 0){
            // Instantiating CategorySeries to plot Pie Chart
            CategorySeries distributionSeries = new CategorySeries("2016");

            // Adding a slice with its values and name to the Pie Chart
            distributionSeries.add(code[1], distribution[1]);


            // Instantiating a renderer for the Pie Chart
            DefaultRenderer defaultRenderer  = new DefaultRenderer();
            SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
            seriesRenderer.setColor(colors[1]);

            // Adding a renderer for a slice
            defaultRenderer.addSeriesRenderer(seriesRenderer);
            defaultRenderer.setLabelsTextSize(75);
            defaultRenderer.setLabelsColor(Color.BLACK);
            if (isDay == true){
                defaultRenderer.setChartTitle("Daily Activity (hours)");
            } else {
                defaultRenderer.setChartTitle("Weekly Activity (hours)");
            }

            defaultRenderer.setChartTitleTextSize(50);
            defaultRenderer.setPanEnabled(false);
            defaultRenderer.setZoomEnabled(false);
            defaultRenderer.setShowLegend(false);
            defaultRenderer.setDisplayValues(true);
            defaultRenderer.setStartAngle(90f);


            // Creating an intent to plot bar chart using dataset and multipleRenderer
            chartView = ChartFactory.getPieChartView(getContext(), distributionSeries, defaultRenderer);

            layout.addView(chartView);
        } else if (cStand == 0) {
            // Instantiating CategorySeries to plot Pie Chart
            CategorySeries distributionSeries = new CategorySeries("2016");

            // Adding a slice with its values and name to the Pie Chart
            distributionSeries.add(code[0], distribution[0]);

            // Instantiating a renderer for the Pie Chart
            DefaultRenderer defaultRenderer  = new DefaultRenderer();
            SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
            seriesRenderer.setColor(colors[0]);

            // Adding a renderer for a slice
            defaultRenderer.addSeriesRenderer(seriesRenderer);
            defaultRenderer.setInScroll(true);
            defaultRenderer.setLabelsTextSize(75);
            defaultRenderer.setLabelsColor(Color.BLACK);
            if (isDay == true){
                defaultRenderer.setChartTitle("Daily Activity (hours)");
            } else {
                defaultRenderer.setChartTitle("Weekly Activity (hours)");
            }
            defaultRenderer.setChartTitleTextSize(50);
            defaultRenderer.setPanEnabled(false);
            defaultRenderer.setZoomEnabled(false);
            defaultRenderer.setShowLegend(false);
            defaultRenderer.setDisplayValues(true);
            defaultRenderer.setStartAngle(90f);

            // Creating an intent to plot bar chart using dataset and multipleRenderer
            chartView = ChartFactory.getPieChartView(getContext(), distributionSeries, defaultRenderer);

            layout.addView(chartView);
        } else {
            // Instantiating CategorySeries to plot Pie Chart
            CategorySeries distributionSeries = new CategorySeries("2016");
            for(int i=0 ;i < distribution.length;i++){

                // Adding a slice with its values and name to the Pie Chart
                distributionSeries.add(code[i], distribution[i]);
            }
//
            // Instantiating a renderer for the Pie Chart
            DefaultRenderer defaultRenderer  = new DefaultRenderer();
            for(int i = 0 ;i<distribution.length;i++){
                SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
                seriesRenderer.setColor(colors[i]);

                // Adding a renderer for a slice
                defaultRenderer.addSeriesRenderer(seriesRenderer);
            }
            defaultRenderer.setLabelsTextSize(75);
            defaultRenderer.setInScroll(true);
            defaultRenderer.setLabelsColor(Color.BLACK);
            if (isDay == true){
                defaultRenderer.setChartTitle("Daily Activity (hours)");
            } else {
                defaultRenderer.setChartTitle("Weekly Activity (hours)");
            }
            defaultRenderer.setChartTitleTextSize(50);
            defaultRenderer.setPanEnabled(false);
            defaultRenderer.setZoomEnabled(false);
            defaultRenderer.setShowLegend(false);
            defaultRenderer.setDisplayValues(true);
            defaultRenderer.setStartAngle(90f);

            // Creating an intent to plot bar chart using dataset and multipleRenderer
            chartView = ChartFactory.getPieChartView(getContext(), distributionSeries, defaultRenderer);

            layout.addView(chartView);



            //Bar Chart
            if (chartView2 != null) {
                layout2.removeView(chartView2);
            }

            if (isDay != true){
                String[] titles = new String[]{
                        "Sitting", "Standing"
                };


                int[] x = { 1,2,3,4,5,6,7 };
                int[] stand = {200,250,270,300,280,350,370};
                int[] sit = {220,270,290,280,260,300,330};

                // Creating an  XYSeries for Income
                XYSeries mStand = new XYSeries("Stand");
                // Creating an  XYSeries for Expense
                XYSeries mSit = new XYSeries("Sit");
                // Adding data to Income and Expense Series
                for(int i=0;i<x.length;i++){
                    mStand.add(x[i], stand[i]);
                    mSit.add(x[i],sit[i]);
                }

                // Creating a dataset to hold each series
                XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
                // Adding Income Series to the dataset
                dataset.addSeries(mStand);
                // Adding Expense Series to dataset
                dataset.addSeries(mSit);

                // Creating XYSeriesRenderer to customize incomeSeries
                XYSeriesRenderer standRenderer = new XYSeriesRenderer();
                standRenderer.setColor(Color.RED);
                standRenderer.setDisplayChartValues(true);
                standRenderer.setChartValuesTextSize(20);
                // Creating XYSeriesRenderer to customize expenseSeries
                XYSeriesRenderer sitRenderer = new XYSeriesRenderer();
                sitRenderer.setColor(Color.BLUE);
                sitRenderer.setDisplayChartValues(true);
                sitRenderer.setChartValuesTextSize(20);

                XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
                multiRenderer.setChartTitle("Weekly Activity");
                multiRenderer.setYTitle("Minutes", 0);
                multiRenderer.setAxisTitleTextSize(50);

                multiRenderer.setXLabelsColor(Color.BLACK);
                multiRenderer.setYLabelsColor(0, Color.BLACK);
                multiRenderer.setYAxisColor(Color.BLACK);
                multiRenderer.setXAxisColor(Color.BLACK);
                multiRenderer.setXLabels(0);
                multiRenderer.setYLabelsAlign(Paint.Align.RIGHT);

                //multiRenderer.setZoomButtonsVisible(true);
                for(int i=0;i<x.length;i++){
                    multiRenderer.addXTextLabel(i+1, days[i]);
                }

                multiRenderer.addSeriesRenderer(standRenderer);
                multiRenderer.addSeriesRenderer(sitRenderer);
                multiRenderer.setZoomEnabled(false, false);
                multiRenderer.setPanEnabled(false);
                multiRenderer.setLegendTextSize(40);
                multiRenderer.setLabelsTextSize(20);
                multiRenderer.setLabelsColor(Color.BLACK);
                multiRenderer.setXAxisMin(0);
                multiRenderer.setXAxisMax(8);
                multiRenderer.setLegendHeight(100);
                multiRenderer.setChartTitleTextSize(50);
                multiRenderer.setMarginsColor(Color.WHITE);
                multiRenderer.setMargins(new int[] {100, 100, 200,0});
                multiRenderer.setBackgroundColor(Color.WHITE);



                chartView2 = ChartFactory.getBarChartView(getContext(), dataset, multiRenderer,
                        BarChart.Type.DEFAULT);

                layout2.addView(chartView2);
            }

        }
    }
}
