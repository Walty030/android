package usydtelcom.electric;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.RunnableFuture;

public class BluetoothActivity extends Activity {

    private BluetoothDevice prevDevice;
    private ConnectedThread mConnectedThread;
    private Globals g;
    private Calendar cal;

    private final static UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"); //Strictly for android devices
    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // Whenever a remote Bluetooth device is found
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // If it's already paired, skip it, because it's been listed already
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    adapter.add(device.getName() + "\n" + device.getAddress());
                }
                // When discovery is finished, change the Activity title
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                setProgressBarIndeterminateVisibility(false);
                setTitle(R.string.select_device);
                if (adapter.getCount() == 0) {
                    String noDevices = getResources().getText(R.string.none_found).toString();
                    adapter.add(noDevices);
                }
            }
        }
    };
    private BluetoothAdapter bluetoothAdapter;
    private ToggleButton toggleButton;
    private ArrayAdapter adapter;
    private static final int ENABLE_BT_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);
        g = (Globals)getApplication();

        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);

        ArrayAdapter<String> pairedDevicesArrayAdapter =
                new ArrayAdapter<String>(this, R.layout.device_name);
        adapter = new ArrayAdapter
                (this, android.R.layout.simple_list_item_1);

        final ListView pairedListView = (ListView) findViewById(R.id.paired_devices);
        pairedListView.setAdapter(pairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(mDeviceClickListener);
        final ListView newDevicesListView = (ListView) findViewById(R.id.new_devices);
        newDevicesListView.setAdapter(adapter);
        newDevicesListView.setOnItemClickListener(mDeviceClickListener);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


        if (bluetoothAdapter != null) {
            Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                findViewById(R.id.title_paired_devices).setVisibility(View.VISIBLE);
                for (BluetoothDevice device : pairedDevices) {
                    pairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
                }
            } else {
                String noDevices = getResources().getText(R.string.none_paired).toString();
                pairedDevicesArrayAdapter.add(noDevices);
            }

            if (bluetoothAdapter.isEnabled()) {
                toggleButton.setChecked(true);

                discoverDevices();
            }
        }
    }
    private String tempMac;
    private AdapterView.OnItemClickListener mDeviceClickListener
            = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
            // Cancel discovery because it's costly and we're about to connect
            bluetoothAdapter.cancelDiscovery();

            // Get the device MAC address, which is the last 17 chars in the View
            String info = ((TextView) v).getText().toString();
            String MAC = info.substring(info.length() - 17);
            //tempMac = MAC;
            // Create the result Intent and include the MAC address
//            BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(MAC);
            g.setTMac(MAC);
            Toast.makeText(getApplicationContext(), g.getTMac(),
                    Toast.LENGTH_SHORT).show();
            // Set result and finish this Activity
//            ConnectingThread t = new ConnectingThread(bluetoothDevice);
//            t.start();
            finish();
        }
    };
    public void onToggleClicked(View view) {

        adapter.clear();

        ToggleButton toggleButton = (ToggleButton) view;

        if (bluetoothAdapter == null) {
            // Device does not support Bluetooth
            Toast.makeText(getApplicationContext(), "Oop! Your device does not support Bluetooth",
                    Toast.LENGTH_SHORT).show();
            toggleButton.setChecked(false);
        } else {

            if (toggleButton.isChecked()){ // to turn on bluetooth
                if (!bluetoothAdapter.isEnabled()) {
                    // A dialog will appear requesting user permission to enable Bluetooth
                    Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBluetoothIntent, ENABLE_BT_REQUEST_CODE);
                } else {
                    Toast.makeText(getApplicationContext(), "Your device has already been enabled." +
                                    "\n" + "Scanning for remote Bluetooth devices...",
                            Toast.LENGTH_SHORT).show();

                    // To discover remote Bluetooth devices
                    discoverDevices();
                }
            } else { // Turn off bluetooth

                bluetoothAdapter.disable();
                adapter.clear();
                Toast.makeText(getApplicationContext(), "Your device is now disabled.",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ENABLE_BT_REQUEST_CODE) {

            // Bluetooth successfully enabled!
            if (resultCode == Activity.RESULT_OK) {
                Toast.makeText(getApplicationContext(), "Ha! Bluetooth is now enabled." +
                                "\n" + "Scanning for remote Bluetooth devices...",
                        Toast.LENGTH_SHORT).show();

                // To discover remote Bluetooth devices
                discoverDevices();

            } else { // RESULT_CANCELED as user refused or failed to enable Bluetooth
                Toast.makeText(getApplicationContext(), "Bluetooth is not enabled.",
                        Toast.LENGTH_SHORT).show();

                // Turn off togglebutton
                toggleButton.setChecked(false);
            }
        }
    }

    protected void discoverDevices(){
        // To scan for remote Bluetooth devices
        if (bluetoothAdapter.startDiscovery()) {
            setProgressBarIndeterminateVisibility(true);
            findViewById(R.id.title_new_devices).setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), "Discovering other bluetooth devices...",
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Discovery failed to start.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Register the BroadcastReceiver for ACTION_FOUND
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(broadcastReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(broadcastReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//        if (id == R.id.nav_settings) {
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }

    private class ConnectingThread extends Thread {
        private final BluetoothSocket bluetoothSocket;
        private final BluetoothDevice bluetoothDevice;

        public ConnectingThread(BluetoothDevice device) {

            BluetoothSocket temp = null;
            bluetoothDevice = device;

            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                temp = bluetoothDevice.createRfcommSocketToServiceRecord(uuid);
            } catch (IOException e) {
                e.printStackTrace();
            }
            bluetoothSocket = temp;
        }

        public void run() {
            // Cancel discovery as it will slow down the connection
            bluetoothAdapter.cancelDiscovery();

            try {
                // This will block until it succeeds in connecting to the device
                // through the bluetoothSocket or throws an exception
                bluetoothSocket.connect();

            } catch (IOException connectException) {
                connectException.printStackTrace();
                try {
                    bluetoothSocket.close();
                } catch (IOException closeException) {
                    Toast.makeText(getApplicationContext(), "Cannot Connect",
                            Toast.LENGTH_SHORT).show();
                    closeException.printStackTrace();
                }
                if (!bluetoothAdapter.isEnabled()){
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Please turn on your bluetooth with the toggle button",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    g.setMac("Invalid");
                }
                return;
            }

            g.setMac(tempMac);
            ConnectedThread s = new ConnectedThread(bluetoothSocket);
            s.start();
        }


        // Cancel an open connection and terminate the thread
        public void cancel() {
            try {
                bluetoothSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private OutputStream mmOutStream;
    private InputStream mmInStream;
    private String inputString;
    private String today;
    private String time;
    private String time2;
    private String past;
    private String tPast;
    private String[] fMatrix;
    private String[] fMatrix2;

    public class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;


            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {

            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;

           // g.setInputStream(mmInStream);
            String s = "S#";
            try {
                tmpOut.write(s.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
            today = g.getToday();
            //if the file was already created we need to calculate the standing and sitting duration
            if (fileExists(getApplicationContext(), "daily.txt")){
                final String duration = readFromFile();
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(getApplicationContext(), duration,
                                Toast.LENGTH_SHORT).show();
                    }
                });
                final String lines[] = duration.split("\n");
//                runOnUiThread(new Runnable() {
//                    public void run() {
//                        Toast.makeText(getApplicationContext(), Integer.toString(lines.length),
//                                Toast.LENGTH_SHORT).show();
//                    }
//                });

                String matrix0[] = lines[0].split("\\s");

                    for (int i=2; i<lines.length; i++){
                        String matrix[] = lines[i-1].split("\\s");
                        String matrix2[] = lines[i].split("\\s");
                        SimpleDateFormat simpleDateFormat =
                                new SimpleDateFormat("HH:mm:ss");

                        try {
                            Date date1 = simpleDateFormat.parse(matrix[0]);
                            Date date2 = simpleDateFormat.parse(matrix2[0]);
                            long difference = date2.getTime() - date1.getTime();
                            if(matrix[1].equals("Stand")){
                                g.setLStand(difference + g.getLStand());
                            } else if (matrix[1].equals("Sit")){
                                g.setLSit(difference + g.getLSit());
                            } else if (matrix[1].equals("Idle")){
                                //TODO
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        past = matrix2[1];
                        tPast = matrix2[0];
                    }

                if (today.equals(matrix0[1])){

                    } else {
                        try {
                           OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("weekly.txt", Context.MODE_PRIVATE));
                           outputStreamWriter.write(matrix0[1] + " " + g.getLStand() + " " + g.getLSit() + "\n");
                           outputStreamWriter.close();
                           } catch (IOException e) {
                           Log.e("Exception", "File write failed: " + e.toString());
                        }
                        g.setLStand(0);
                        g.setLSit(0);
                        File dir = getFilesDir();
                        File d = new File(dir, "daily.txt");
                        d.delete();

                        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            Date checker = date.parse(matrix0[1]);
                            if (!isDateInCurrentWeek(checker)){
                                File w = new File(dir, "daily.txt");
                                w.delete();
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

            }

            g.setOutputStream(mmOutStream);
        }

        public void run(){

            while (true){


                try {
                    byte[] buffer = new byte[128];
                    String readMessage;
                    String pres;
                    int bytes;
                    if (mmInStream.available()>1) {
                        try {
                            // Read from the InputStream
                            bytes = mmInStream.read(buffer);
                            readMessage = new String(buffer, 0, bytes);
                            inputString = readMessage.trim();
                            cal = Calendar.getInstance();
                            int hour = cal.get(Calendar.HOUR_OF_DAY);
                            int minute = cal.get(Calendar.MINUTE);
                            int second = cal.get(Calendar.SECOND);
                            time = Integer.toString(hour)+":"+Integer.toString(minute)+":"+Integer.toString(second);

                            if(inputString.equals("I")){
                                pres = "Idle";
                                time2 = time + " Idle";
                            } else if(Integer.parseInt(inputString)>79){
                                pres = "Stand";
                                time2 = time + " Stand";
                            } else {
                                pres = "Sit";
                                time2 = time + " Sit";
                            }
                            if (!fileExists(getApplicationContext(), "daily.txt")) {
                                try {
                                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("daily.txt", Context.MODE_PRIVATE));
                                    outputStreamWriter.write("Daily " + today + "\n");
                                    outputStreamWriter.write(time2 + "\n");
                                    outputStreamWriter.close();
                                } catch (IOException e) {
                                    Log.e("Exception", "File write failed: " + e.toString());
                                }
                                //pres = time;
                            } else {
                                try {
                                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("daily.txt", Context.MODE_APPEND));
                                    outputStreamWriter.write(time2+"\n");
                                    outputStreamWriter.close();
                                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
                                    try {
                                        Date date1 = simpleDateFormat.parse(tPast);
                                        Date date2 = simpleDateFormat.parse(time);
                                        long difference = date2.getTime() - date1.getTime();
                                        if(past.equals("Stand")){
                                            g.setLStand(difference + g.getLStand());
                                        } else if (past.equals("Sit")){
                                            g.setLSit(difference + g.getLSit());
                                        } else if (past.equals("Idle")){
                                            //TODO
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                                catch (IOException e) {
                                    Log.e("Exception", "File write failed: " + e.toString());
                                }
                            }
                            past = pres;
                            tPast = time;
                            g.setInputString(inputString);

                        } catch (IOException e) {
                            //Log.e(TAG, "disconnected", e);
                            break;
                        }

                    }    else
                        SystemClock.sleep(100);

                } catch (IOException e) {

                    e.printStackTrace();
                }
            }
        }
    }


    public boolean fileExists(Context context, String filename) {
        File file = context.getFileStreamPath(filename);
        if(file == null || !file.exists()) {
            return false;
        }
        return true;
    }

    private String readFromFile() {

        String ret = "";

        try {
            InputStream inputStream = openFileInput("daily.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString + "\n");
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public static boolean isDateInCurrentWeek(Date date) {
        Calendar currentCalendar = Calendar.getInstance();
        int week = currentCalendar.get(Calendar.WEEK_OF_YEAR);
        int year = currentCalendar.get(Calendar.YEAR);
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);
        int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR);
        int targetYear = targetCalendar.get(Calendar.YEAR);
        return week == targetWeek && year == targetYear;
    }

}
