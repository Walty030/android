package usydtelcom.electric;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.Toast;


public class DSettingFragment extends Fragment {
    Globals g;
    private changeFragmentListener gB;
    public interface changeFragmentListener {
        public void goBack();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        g = (Globals)getActivity().getApplication();
        getActivity().setTitle("Desk Settings");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_dsetting, container, false);
        final NumberPicker np1 = (NumberPicker)view.findViewById(R.id.numberPicker1);
        final NumberPicker np2 = (NumberPicker)view.findViewById(R.id.numberPicker2);
        ImageButton dBack = (ImageButton)view.findViewById(R.id.imageDBack);
        ImageButton dApply = (ImageButton)view.findViewById(R.id.imageDApply);
        np1.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        np2.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        dBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                gB = (changeFragmentListener)getActivity();
                gB.goBack();
            }
        });

        dApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                g.setSittingHeight(Integer.toString(np1.getValue()));
                g.setStandingHeight(Integer.toString(np2.getValue()));

                Toast.makeText(getContext(), "Saved Settings",
                        Toast.LENGTH_SHORT).show();

                gB = (changeFragmentListener)getActivity();
                gB.goBack();
            }
        });

        np1.setMinValue(60);
        np1.setMaxValue(79);

        np2.setMinValue(80);
        np2.setMaxValue(120);

        np1.setValue(Integer.parseInt(g.getSittingHeight()));
        np2.setValue(Integer.parseInt(g.getStandingHeight()));

        return view;
    }

}
